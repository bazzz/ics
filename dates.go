package ics

// ISO8601Date represents a data in format 'yyyy-MM-dd' by returning Go format '2006-01-02'.
const ISO8601Date string = "2006-01-02"

// ISO8601Time represents a time in format 'HH:mm:ss' by returning Go format '15:04:05'.
const ISO8601Time string = "15:04:05"

// ISO8601DateTime represents a date + time in format 'yyyy-MM-dd HH:mm:ss' by returning Go format '2006-01-02 15:04:05'.
const ISO8601DateTime string = "2006-01-02 15:04:05"
