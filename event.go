package ics

import (
	"strings"
	"time"

	ical "github.com/arran4/golang-ical"
)

// Event is a calendar event.
type Event struct {
	UID           string
	Timestamp     time.Time
	Organizer     string
	Attendee      string
	Description   string
	Categories    string
	Class         string
	Created       string
	Summary       string
	Start         time.Time
	End           time.Time
	Location      string
	Status        string
	FreeBusy      string
	LastModified  time.Time
	URL           string
	RepeatRule    string
	ExcludedDates []time.Time
}

func (e Event) String() string {
	return e.Start.Format(ISO8601DateTime) + " " + e.End.Format(ISO8601DateTime) + " " + e.Summary + " " + e.Location
}

func newEvent(e *ical.VEvent) Event {
	return Event{
		UID:           getString(e, ical.ComponentPropertyUniqueId),
		Timestamp:     getTime(e, ical.ComponentPropertyDtstamp),
		Organizer:     getString(e, ical.ComponentPropertyOrganizer),
		Attendee:      getString(e, ical.ComponentPropertyAttendee),
		Description:   getString(e, ical.ComponentPropertyDescription),
		Categories:    getString(e, ical.ComponentPropertyCategories),
		Class:         getString(e, ical.ComponentPropertyClass),
		Created:       getString(e, ical.ComponentPropertyCreated),
		Summary:       getString(e, ical.ComponentPropertySummary),
		Start:         getTime(e, ical.ComponentPropertyDtStart),
		End:           getTime(e, ical.ComponentPropertyDtEnd),
		Location:      getString(e, ical.ComponentPropertyLocation),
		Status:        getString(e, ical.ComponentPropertyStatus),
		FreeBusy:      getString(e, ical.ComponentPropertyFreebusy),
		LastModified:  getTime(e, ical.ComponentPropertyLastModified),
		URL:           getString(e, ical.ComponentPropertyUrl),
		RepeatRule:    getRepeatRule(e),
		ExcludedDates: getExcludedDates(e),
	}
}

func getString(e *ical.VEvent, p ical.ComponentProperty) string {
	v := e.GetProperty(p)
	if v == nil {
		return ""
	}
	return strings.Replace(v.Value, `\,`, `,`, -1)
}

func getTime(e *ical.VEvent, p ical.ComponentProperty) time.Time {
	v := e.GetProperty(p)
	if v == nil {
		return time.Time{}
	}

	return parseTimeAndLocation(v)
}

func parseTimeAndLocation(v *ical.IANAProperty) time.Time {
	// Searching location, for instance "Europe/Amsterdam".
	loc := ""
	for k, p := range v.ICalParameters {
		if k == "TZID" {
			loc = p[0]
			break
		}
	}

	// Some ICS clients seem to use non IANA time zone identifiers. There is probably no way to solve this other than doing replacements.
	if loc == "W. Europe Standard Time" {
		loc = "Europe/Amsterdam"
	}

	location, err := time.LoadLocation(loc)
	if err != nil {
		return time.Time{}
	}

	return parseTime(v.Value, location)
}

func parseTime(value string, location *time.Location) time.Time {
	pattern := "20060102T150405"
	if len(value) == 8 {
		pattern = "20060102"
	} else if len(pattern) < len(value) {
		pattern += value[len(pattern):] // Remove end cruft.
	}

	t, err := time.ParseInLocation(pattern, value, location)
	if err != nil {
		return time.Time{}
	}
	return t
}

func getRepeatRule(e *ical.VEvent) string {
	for _, prop := range e.Properties {
		if prop.IANAToken == "RRULE" {
			return prop.Value
		}
	}
	return ""
}

func getExcludedDates(e *ical.VEvent) []time.Time {
	dates := make([]time.Time, 0)
	for _, prop := range e.Properties {
		if prop.IANAToken == "EXDATE" {
			date := parseTimeAndLocation(&prop)
			dates = append(dates, date)
		}
	}
	return dates
}
