package ics

import (
	"crypto/tls"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	linq "github.com/ahmetb/go-linq"
	ical "github.com/arran4/golang-ical"
)

// GetEvents returns all events for the specified url having their starting moment between startDate and endDate.
func GetEvents(url string, username string, password string, startDate time.Time, endDate time.Time) []Event {
	client := &http.Client{Transport: &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}}
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}
	request.SetBasicAuth(username, password)

	response, err := client.Do(request)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()

	// Get all events from the calendar.
	cal, err := ical.ParseCalendar(response.Body)
	if err != nil {
		panic(err)
	}
	events := make([]Event, 0)
	for _, calEvent := range cal.Events() {
		event := newEvent(calEvent)
		expandedEvents := expandRepeatingEvent(event, endDate)
		events = append(events, expandedEvents...)
	}

	// Filter out events that are not between the requested dates.
	filteredEvents := make([]Event, 0)
	for _, event := range events {
		if startDate.Sub(event.Start) < 0 && endDate.Sub(event.End) > 0 {
			filteredEvents = append(filteredEvents, event)
		}
	}
	linq.From(filteredEvents).
		OrderByT(func(x Event) string { return x.Start.Format(ISO8601DateTime) }).
		ToSlice(&filteredEvents)
	return filteredEvents
}

// expandRepeatingEvent changes a repeating event into multiple seperate Events. If the provided event does not repeat is it returned as the only event.
func expandRepeatingEvent(event Event, endDate time.Time) []Event {
	events := make([]Event, 0)
	if event.RepeatRule == "" || !strings.HasPrefix(event.RepeatRule, "FREQ=") {
		events = append(events, event)
		return events
	}

	freq := ""
	var count int
	var interval int
	var until time.Time
	components := strings.Split(event.RepeatRule, ";")
	for _, component := range components {
		prefix := "FREQ="
		if strings.HasPrefix(component, prefix) {
			freq = component[len(prefix):]
			continue
		}
		prefix = "COUNT="
		if strings.HasPrefix(component, prefix) {
			countInt, err := strconv.Atoi(component[len(prefix):])
			if err != nil {
				panic(err)
			}
			count = countInt
			continue
		}
		prefix = "INTERVAL="
		if strings.HasPrefix(component, prefix) {
			intervalInt, err := strconv.Atoi(component[len(prefix):])
			if err != nil {
				panic(err)
			}
			interval = intervalInt
			continue
		}
		prefix = "UNTIL="
		if strings.HasPrefix(component, prefix) {
			until = parseTime(component[len(prefix):], time.Local)
			continue
		}
		prefix = "BYMONTHDAY="
		if strings.HasPrefix(component, prefix) {
			// No need to do anything. The day is already specified by Start.
			continue
		}
		prefix = "BYDAY="
		if strings.HasPrefix(component, prefix) && strings.Contains(event.RepeatRule, "FREQ=WEEKLY") {
			// No need to do anything. The day is irrelevant if freq is weekly because it will always be 7 days and therefore the same day.
			continue
		}
		log.Println("Unknown RRULE component:", component, "in", event.RepeatRule)
	}

	// if until is not set or endDate is earlier than until, then endDate is the until.
	if until.Year() == 1 || endDate.Sub(until) < 0 {
		until = endDate
	}

	switch freq {
	case "DAILY":
		days := 1
		if interval > 0 {
			days *= interval
		}
		clones := generateClones(event, until, count, 0, 0, days)
		events = append(events, clones...)
	case "WEEKLY":
		days := 7
		if interval > 0 {
			days *= interval
		}
		clones := generateClones(event, until, count, 0, 0, days)
		events = append(events, clones...)
	case "MONTHLY":
		months := 1
		if interval > 0 {
			months *= interval
		}
		clones := generateClones(event, until, count, 0, months, 0)
		events = append(events, clones...)
	case "YEARLY":
		years := 1
		if interval > 0 {
			years *= interval
		}
		clones := generateClones(event, until, count, years, 0, 0)
		events = append(events, clones...)
	}
	return events
}

func generateClones(event Event, until time.Time, count int, years int, months int, days int) []Event {
	clones := make([]Event, 0)
	start := event.Start
	end := event.End
	addCount := 0

	for until.Sub(start) > 0 && (count == 0 || addCount < count) {
		// Do not make a clone for a date that is in excluded dates.
		if DateSliceContains(event.ExcludedDates, start) {
			start = start.AddDate(years, months, days)
			end = end.AddDate(years, months, days)
			continue
		}
		clone := Event{
			Organizer:   event.Organizer,
			Attendee:    event.Attendee,
			Description: event.Description,
			Categories:  event.Categories,
			Class:       event.Class,
			Summary:     event.Summary,
			Start:       start,
			End:         end,
			Location:    event.Location,
			Status:      event.Status,
			FreeBusy:    event.FreeBusy,
			URL:         event.URL,
		}
		clones = append(clones, clone)
		start = start.AddDate(years, months, days)
		end = end.AddDate(years, months, days)
		addCount++
	}
	return clones
}
