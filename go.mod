module gitlab.com/bazzz/ics

go 1.16

require (
	github.com/ahmetb/go-linq v3.0.0+incompatible
	github.com/arran4/golang-ical v0.0.0-20210825232153-efac1f4cb8ac
)
