package ics

import "time"

// DateContains returns a boolean indicating whether any date part of time.Time slice contains the date part of value.
func DateSliceContains(slice []time.Time, value time.Time) bool {
	for _, content := range slice {
		if content.Year() == value.Year() && content.Month() == value.Month() && content.Day() == value.Day() {
			return true
		}
	}
	return false
}
